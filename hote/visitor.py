#!/usr/bin/python2

# -*- coding: utf-8 -*-

from AST import *
import sys

code = {
		'include' : 
		"""
		#include <stdio.h>
		#include <stdlib.h>
		#include <unistd.h>
		#include <errno.h>
		#include <sys/types.h>
		#include <sys/stat.h>
		#include <sys/wait.h>
		#include <sys/socket.h>
		#include <sys/un.h>
		#include <arpa/inet.h>
		#include <fcntl.h>
		#include <string.h>
		#include "/opt/libblare/lib.h"


		int main(void) {
		""",

		'end' : 
		"""
		return EXIT_SUCCESS;
		}""",

		'declaration':
		"""
		//declarations
		int s;
		pid_t pid, child;
		int proc;
		char str[80];
		char ret[80];
		char *arg[3];
		int pipefd[2];
		int i, j, n;
		char *filename;
		struct tags_t tags;
		struct policy_t policy;
		""",

		'initialisation':
		"""
		//init
		""",

		'dec' : lambda x,y : 
		"""
		%s %s;
		""" % (type_var[x], y),

		'init' : {
			'file' : lambda x : 
			"""
			%s = creat("%s", S_IRWXU | S_IRWXG | S_IRWXO);
			if (%s < 0) {
				printf("Error\\n");
			}
                        write(%s, "%s", sizeof("%s"));
			close(%s);
			""" % (x, x, x, x, x, x, x),

			'socket_i' : lambda x :
			"""
			child = fork();
			if (0 == child) {
				struct sockaddr_in server;
				int out_%s;
				out_%s = socket(AF_INET , SOCK_STREAM , 0);
				server.sin_addr.s_addr = inet_addr("127.0.0.1");
				server.sin_family = AF_INET;
				server.sin_port = htons( %s );
				connect(out_%s , (struct sockaddr *)&server , sizeof(server));
			""" % (x, x, x, x),

			'socket_u' : lambda x :
			"""
			child = fork();
			if (0 == child) {
				struct sockaddr_un server;
				int %s, len;
				sleep(3);
				%s = socket(AF_UNIX , SOCK_STREAM , 0);
				server.sun_family = AF_UNIX;
				strcpy(server.sun_path, "%s");
				len = strlen(server.sun_path) + sizeof(server.sun_family);
				connect(%s , (struct sockaddr *)&server, len);
			""" % (x, x, x, x),

			'socket_a' : lambda nop :
			"""
			int socks[2];
			socketpair(AF_UNIX, SOCK_STREAM, 0, socks);
			child = fork();
			if (0 == child) {
				close(socks[1]);
			"""
			},

		'run' : 
			"""
		//run tests
		pid = getpid();
		""",

	'tag' : {
			'get' : {
				'info' : lambda x, y :
				"""
				filename = "%s";
				n=get_tags_from_file(filename, INFO_XATTR, &tags);
				if (n<0) {
					strcpy(ret, "not present");
				} else {
					n=0;
					for(i=0 ; i<tags.size ; i++) {
						n+= sprintf(&ret[n], "%%d ", tags.values[i]);
					}
					ret[n-1]=0;
					free_tags(&tags);
				}
				""" % (x),

				'policy' : lambda x, y:
				"""
				filename = "%s";
				n=get_policy_from_file(filename, &policy, %s);
				if (n<0) {
					strcpy(ret, "not present");
				} else {
					n=0;
					for(i=0; i<policy.size; i++) {
						ret[n++]='[';
						for(j=0; j<policy.tags[i].size; j++) {
							n+= sprintf(&ret[n], "%%d ", policy.tags[i].values[j]);
						}
						ret[n-1]=']';
						ret[n++]=' ';
					}
					ret[n-1]=0;
					free_policy(&policy);
				}
				""" % (x,y)
				},

			'set' : {
				'info' : lambda v, w, x, y, z :
				"""
				filename = "%s";
				char *%s[] = {"1","2","%s"};
				get_tags_from_command(%s,%s,&tags);
				write_tags(filename, INFO_XATTR, tags);
				free_tags(&tags);
				""" % (v, w, y, x, w),

				'policy' : lambda v, w, x, y, z :
				"""
				filename = "%s";
				char *%s[] = {"1","2","%s"};
				get_policy_from_command(%s, %s, &policy);
				write_policy(filename, policy, %s);
				free_policy(&policy);
				""" % (v, w, y, x, w, z)
				}

			},

	'function' : {
			'open' : lambda x : x != "" and
			"""
			%s = open("%s", O_RDWR);
			if (%s == -1) {
				perror("can't open %s");
				exit(EXIT_FAILURE);
			}
			""" % (x, x, x, x),

			'read' : lambda x :
			"""
			s = read(%s, str, 80);
			if (s == -1) {
				perror("can't read %s");
				exit(EXIT_FAILURE);
			}
			""" % (x, x),

			'write' : lambda x :
			"""
			s = write(%s, str, 80);
			if (s == -1) {
				perror("can't write %s");
				exit(EXIT_FAILURE);
			}
			""" % (x, x),

			'close' : lambda nop, x :
			"""
			close(%s);
			""" % (x,),

			'send_i': lambda x :
			"""
			send(out_%s , str , strlen(str) , 0);
			close(out_%s);
			return;
			}
			struct sockaddr_in server, client;
			int out_%s, in_%s, c;
			out_%s = socket(AF_INET, SOCK_STREAM, 0);
			server.sin_family = AF_INET;
			server.sin_addr.s_addr = INADDR_ANY;
			server.sin_port = htons( %s );
			bind(out_%s,(struct sockaddr *)&server , sizeof(server));
			listen(out_%s, 3);
			c = sizeof(struct sockaddr_in);
			in_%s = accept(out_%s, (struct sockaddr *)&client, (socklen_t*)&c);
			""" % (x, x, x, x, x, x, x, x, x, x),

			'send_u': lambda x :
			"""
			send(%s , str , strlen(str) , 0);
			close(%s);
			return;
			}
			struct sockaddr_un server, client;
			int out_%s, in_%s, c, len;
			out_%s = socket(AF_UNIX, SOCK_STREAM, 0);
			server.sun_family = AF_UNIX;
			strcpy(server.sun_path, "%s");
			unlink(server.sun_path);
			len = strlen(server.sun_path) + sizeof(server.sun_family);
			bind(out_%s,(struct sockaddr *)&server , sizeof(server));
			listen(out_%s, 3);
			c = sizeof(struct sockaddr_un);
			in_%s = accept(out_%s, (struct sockaddr *)&client, (socklen_t*)&c);
			""" % (x, x, x, x, x, x, x, x, x, x),

			'send_a': lambda nop :
			"""
			write(socks[0] , str , strlen(str));
			close(socks[0]);
			return;
			}
			close(socks[0]);
			pid = getpid();
			""",

			'recv': lambda x :
			"""
                        sleep(1);
			recv(in_%s, str, 80 , 0);
			close(out_%s);
			close(in_%s);
			""" % (x, x, x),

			'recv_a': lambda nop :
			"""
			read(socks[1], str, 80);
			close(socks[1]);
			"""

			},

	'query' : {
			'function' : lambda x, y : 
			"""
			sprintf(str, "/proc/%%d/attr/%s", pid);
			proc = open(str, O_RDONLY);
			s = read(proc, ret, 80);
			if (strcmp(ret,"%s") == 0) {
				printf("SUCCESS\\n");
				printf("%%d: tags are '%%s'\\n", pid, ret);
			}
			else {
				printf("FAILURE\\n");
				printf("%%d: tags are '%%s' and not '%s'\\n", pid, ret);
			}
			close(proc);
			""" % (x, y, y),

			'var' : lambda x, y:
			"""
			if (strcmp(ret, "%s") == 0) {
				printf("SUCCESS\\n");
				printf("%s: tags are '%%s'\\n", ret);
			}
			else {
				printf("FAILURE\\n");
				printf("%s: tags are '%%s' and not '%s'\\n", ret);
			}
			""" % (x, y, y, x)
			}
	}

prog_dir = "/opt/libblare/"

var = []
cmd_set = {
		'itag' : prog_dir + "setinfo",
		'ptag' : prog_dir + "setpolicy",
		'xptag' : prog_dir + "setxpolicy"
		}

cmd_get = {
		'itag' : prog_dir + "getinfo",
		'ptag' : prog_dir + "getpolicy",
		'xptag' : prog_dir + "getxpolicy"
		}

tag_type = {
		'itag' : 'info',
		'ptag' : 'policy',
		'xptag' : 'policy'
		}

xattr_name = {
		'itag' : 'INFO',
		'ptag' : 'POLICY',
		'xptag' : 'XPOLICY'
		}

type_var = {
		'file' : "int",
		'socket_i' : "//int",
		'socket_u' : "//int",
		'socket_a' : "//int"
		}



class Visitor(object):
	n_arg=0
	def __init__(self):
		self.first_pass=True
		self.query=False

	def visitProgram(self, p):
		print code['include']
		for i in p.children:
			i.accept(self)
		print code['end']

	def visit(self, o, filename):
		program = open(filename, 'w')
		sys.stdout = program
		o.accept(self)

	def visitContainer(self, c):
		print code['declaration']
		for i in c.children:
			i.accept(self)
		self.first_pass=False
		print code['initialisation']
		for i in c.children:
			i.accept(self)

	def visitRun(self, r):
		print code['run']
		for i in r.children:
			i.accept(self)

	def visitDec(self, d):
		if (self.first_pass):
			print code['dec'](d.var.typeVar, d.var.name)
			var.append(d.var)
		else:
			print code['init'][d.var.typeVar](d.var.name)
			if d.children != None:
				for i in d.children:
					i.accept(self)
					print ""

	def visitTag(self, t):
		if self.query:
			print code['tag']['get'][tag_type[t.typeTag]](t.parent.var.name, xattr_name[t.typeTag])
		else:
			print code['tag']['set'][tag_type[t.typeTag]](t.parent.var.name, "arg" + str(Visitor.n_arg), 3+t.__str__().count(" "), t.__str__().replace("[","{").replace("]","}").replace(" ","\",\""), xattr_name[t.typeTag])
			Visitor.n_arg+=1

	def visitFunction(self, f):
		print """//begin syscall"""
		print code['function'][f.name](f.att.name)
		print """//end syscall\n"""

	def visitQuery(self, q):
		tags = {
				'itag':"current",
				'ptag':"prev",
				'xtag':"exec"
				}
		self.query = True
		if isinstance(q.var, FunctionNode):
			q.var.accept(self)
			for i in q.tags:
				print code['query']['function'](tags[i.typeTag], i.typeTag + "s : " + str(i))
		elif isinstance(q.var, VarNode):
			for i in q.tags:
				i.accept(self)
				print code['query']['var'](str(i), i.parent.var.name)
		self.query = False

	def visitVar(self, v):
		if not(v in var):
			print "Error !"
			exit()
		print v.name
