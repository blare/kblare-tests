#!/usr/bin/python2

# -*- coding: utf-8 -*-

class Node(object):
	type = 'Node'
	def __init__(self, children=None, parent=None):
		self.children = children
		self.parent = parent

	def __repr__(self):
		return self.type

class ProgramNode(Node):
	type='Program'

	def accept(self, visitor):
		visitor.visitProgram(self)

class ContainerNode(Node):
	type='Container'

	def accept(self, visitor):
		visitor.visitContainer(self)

class RunNode(Node):
	type='Run'

	def accept(self, visitor):
		visitor.visitRun(self)

class DecNode(Node):
	type='Dec'

	def __init__(self, children=None, var=None, typeVar=None):
		super(DecNode, self).__init__(children)
		self.var = var
		self.typeVar = typeVar

	def accept(self, visitor):
		visitor.visitDec(self)

class TagNode(Node):
	type='tag'

	def __init__(self, children=None, typeTag=None):
		super(TagNode, self).__init__(children)
		self.typeTag = typeTag

	def accept(self, visitor):
		visitor.visitTag(self)

	def __str__(self):
		ch = ""
		if type(self.children) == type([]):
			if type(self.children[0]) == type([]):
				for i in self.children:
					ch += "[" + " ".join([str(j) for j in i]) + "]"

					if i != self.children[-1:][0]:
					   ch += " "

			else:
				ch += " ".join([str(i) for i in self.children])

		return ch

class QueryNode(Node):
	type='Query'

	def __init__(self, children=None, var=None, tags=None):
		super(QueryNode, self).__init__(children)
		self.tags = tags
		self.var = var

	def accept(self, visitor):
		visitor.visitQuery(self)

class FunctionNode(Node):
	type='Function'

	def __init__(self, children=None, name=None, att=None):
		super(FunctionNode, self).__init__(children)
		self.name = name
		self.att = att


	def accept(self, visitor):
		visitor.visitFunction(self)

class VarNode(Node):
	type='Var'

	def __init__(self, children=None, name=None, typeVar=None, dec=False):
		super(VarNode, self).__init__(children)
		self.name = name
		self.typeVar = typeVar
		self.dec = dec

	def accept(self, visitor):
		visitor.visitVar(self)

	def __cmp__(self, var):
		return cmp(self.name, var.name)
