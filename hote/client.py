#!/usr/bin/python2
# -*- coding: utf-8 -*-

import socket
import sys
import os
from subprocess import *
import time
import threading

exit_status=0

def end(sock):
	try:
		sock.send("9\ntype:end\n")
		sys.stdout.write(sock.recv(1024))
	except:
		print "end"
		restart_vm()
		exit(-1)

def send_file(sock, f):
	fd = open(f,'r')
	ch = "type:file\ncontent:%s\n" %(f,) + fd.read()
	try:
		sock.send("%d\n%s" % (len(ch), ch))
		tmp = ""
		while not tmp.endswith('type:ack\n'):
			tmp = ""
			while True:
				char = sock.recv(1)
				tmp = tmp + char
				if char == '\n':
					if not tmp.endswith('type:ack\n'):
						sys.stdout.write(tmp)
					break
	except:
		print "send_file"
		restart_vm()
		exit(-1)

def execute(sock, f):
	ch="type:execute\ncontent:%s\n" % (f,)
	exit_status = 0
	try:
		sock.send("%d\n%s" % (len(ch), ch))
		tmp = ""
		while not tmp.endswith('type:ack\n'):
			tmp = ""
			while True:
				char = sock.recv(1)
				tmp = tmp + char
				if char == '\n':
					if not tmp.endswith('type:ack\n'):
						sys.stdout.write(tmp)
						if tmp.endswith('FAILURE\n'):
							exit_status=-1
					break
		return exit_status
	except:
		print "execute"
		restart_vm()
		exit(-1)

def restart_vm():
	print "vm restarting..."


if __name__ == '__main__':
	if len(sys.argv) < 3:
		print "Missing arguments: client.py <HOST> <PORT>"
		sys.exit(-1)
	else:
		HOST = sys.argv[1]
		PORT = int(sys.argv[2])

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
	sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 2)
	sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 2)
	sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 2)
	sock.connect((HOST, PORT))

	try:
		os.mkdir('generated')
	except:
		pass
	for i in os.listdir('tests'):
		if i.find('.bl') != -1:
			os.system("python2 parse.py tests/%s" % (i,))
			pass

	os.chdir("tests")

	print "begin test keepalive"

	exit_status = 0
	for i in os.listdir('.'):
		if i.find('.c') != -1:
			send_file(sock, i)
			tmp = execute(sock, i[:-2])
			if tmp == -1:
				exit_status = -1

	end(sock)
	print "end sent..."

	sock.close()
	exit(exit_status)

