#!/usr/bin/python2

# -*- coding: utf-8 -*-

import ply.yacc as yacc
from lex import tokens
from AST import *
from visitor import *

def p_program(p):
	"""program : containers run"""
	p[0] = ProgramNode([p[1], p[2]])

def p_containers(p):
	"""containers : CONTAINER LEFT_B declarations RIGHT_B"""
	p[0] = ContainerNode(p[3])

def p_declarations(p):
	"""declarations : dec declarations
	| dec"""
	try:
		p[0] = [p[1]] + p[2]
	except:
		p[0] = [p[1]]

def p_dec(p):
	"""dec : TYPE VAR COLON tags SEMICOLON
	| TYPE VAR SEMICOLON
        | TYPE NUMBER COLON tags SEMICOLON
        | TYPE NUMBER SEMICOLON"""
	try:
		p[0] = DecNode(var = VarNode(name=p[2], typeVar=p[1]), children=p[4])
		for i in p[4]:
			i.parent = p[0]
	except:
		p[0] = DecNode(var = VarNode(name=p[2], typeVar=p[1]), children=None)

def p_tags(p):
	"""tags : tag COMMA tags
	| tag"""
	try:
		p[0] = [p[1]] + p[3]
	except:
		p[0] = [p[1]]

def p_tag(p):
	"""tag : ITAG LEFT_P list RIGHT_P
	| PTAG LEFT_P dlist RIGHT_P
	| XPTAG LEFT_P dlist RIGHT_P"""
	p[0] = TagNode(typeTag=p[1], children=p[3])

def p_list(p):
	"""list : NUMBER list
	| NUMBER"""
	try:
		p[0] = [p[1]] + p[2]
	except:
		p[0] = [p[1]]

def p_dlist(p):
	"""dlist : LEFT_P list RIGHT_P dlist
	| LEFT_P list RIGHT_P"""
	try:
		p[0] = [p[2]] + p[4]
	except:
		p[0] = [p[2]]

def p_run(p):
	"""run : RUN LEFT_B statements RIGHT_B"""
	p[0] = RunNode(p[3])

def p_statements(p):
	"""statements : statement statements
	| statement"""
	try:
		#p[0] = p[1] + "\n" + p[2]
		p[0] = [p[1]] + p[2]
	except:
		#p[0] = p[1]
		p[0] = [p[1]]

def p_query(p):
	"""statement : function QUERY tags"""
	p[0] = QueryNode(var=p[1], tags=p[3])

def p_query_var(p):
	"""statement : VAR QUERY tags"""
	p[0] = QueryNode(var=VarNode(name=p[1]), tags=p[3])
	for i in p[3]:
		i.parent = p[0]

def p_statement(p):
	"""statement : function"""
	p[0] = p[1]

def p_function(p):
	"""function : FCT LEFT_P att RIGHT_P"""
	p[0] = FunctionNode(name=p[1], att=p[3])

def p_att(p):
	"""att : VAR
        | NUMBER
	| """
	try :
		p[0] = VarNode(name=p[1])
	except:
		p[0] = str(p[2])

def p_error(p):
	if (p != None):
		print "Syntax error in line %d" % (p.lineno,)
		yacc.errok()
	else:
		print "Can't generate C code !"
		exit(0)

yacc.yacc(outputdir="generated", debug=1)

if __name__=="__main__":
	import sys
	prog = file(sys.argv[1]).read()
	result = yacc.parse(prog)

	visitor = Visitor()

	visitor.visit(result, sys.argv[1][:-3] + '.c')
