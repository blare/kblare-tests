#!/usr/bin/python2

# -*- coding: utf-8 -*-

import ply.lex as lex

tokens = (
		'CONTAINER',
		'RUN',
		'LEFT_P',
		'RIGHT_P',
		'SEMICOLON',
		'VAR',
		'FCT',
		'NUMBER',
		'LEFT_B',
		'RIGHT_B',
		'COLON',
		'COMMA',
		'ITAG',
		'PTAG',
		'XPTAG',
		'TYPE',
		'QUERY',
		)

t_LEFT_P = r"\("
t_RIGHT_P = r"\)"
t_LEFT_B = r"{"
t_RIGHT_B = r"}"

t_SEMICOLON = r";"
t_COLON = r":"
t_COMMA = r","
t_QUERY = r"\?"

t_VAR = r"[a-zA-Z_]+"

t_ignore = r" "

def t_TYPE(t):
	r"file|socket_i|socket_u|socket_a"
	t.value = str(t.value)
	return t

def t_ITAG(t):
	r"itag"
	t.value = str(t.value)
	return t

def t_PTAG(t):
	r"ptag"
	t.value = str(t.value)
	return t

def t_XPTAG(t):
	r"xptag"
	t.value = str(t.value)
	return t

def t_CONTAINER(t):
	r"container"
	t.value = str(t.value)
	return t

def t_RUN(t):
	r"run"
	t.value = str(t.value)
	return t

def t_FCT(t):
	r"open|read|write|close|send_i|send_u|send_a|recv_a|recv"
	t.value = str(t.value)
	return t

def t_NUMBER(t):
	r"\d+(\.+\d+)*"
	try:
		t.value = int(t.value)
	except:
		t.value = float(t.value)

	return t

def t_newline(t):
	r"\n+"
	t.lexer.lineno += len(t.value)

def t_error(t):
	print "error, illegal character : %s" %(t.value[0])
	t.lexer.skip(1)

lex.lex()

if __name__ == "__main__":
	import sys
	prog = file(sys.argv[1]).read()

	lex.input(prog)

	while 1:
		tok = lex.token()
		if not tok: break

		print "line (%d) : %s(%s)" % (tok.lineno, tok.type, tok.value)
