#!/usr/bin/python
# -*- coding: utf-8 -*-

import socket
import sys
import os
from subprocess import *
import time
import io

address = ('', 1337)

def parse(req, st):
	line = st.split('\n')
	if line[0].find('file') != -1:
		name = line[1].split(':')[1]
		print name
		f = open(name, 'w')
		f.write('\n'.join(line[2:]))
		f.close()
		req.send("received: %s\n" % name)
		os.system("gcc -o %s %s /opt/libblare/lib.o" % (name[:-2], name))
		req.send("compiled: %s\n" % name)
		req.send('type:ack\n')

	elif line[0].find('end') != -1:
		print "stopping server"
		req.send("stopping server\n")
		req.close()
		server.close()
		exit(0)

	elif line[0].find('execute') != -1:
		print "execute test"
		name = line[1].split(':')[1]
		print name
		req.send('execute: %s\n' % name)
		call(['./' + name], stdout=req.fileno())
		req.send('type:ack\n')
	else:
		print "st : " + st
		req.send("Command not supported ")
		req.close()

def handle(sock):
	ch = ""

	while True:
		temp = sock.recv(1)
		if temp == '\n':
			break
		else:
			ch = ch + temp

	parse(sock, sock.recv(int(ch)))
	#print ch


if __name__ == '__main__':

	try:
		os.chdir('tests')
	except:
		os.mkdir('tests')
		os.chdir('tests')
	for f in os.listdir('.'):
		os.remove(f)

	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	server.bind(address)
	print "server launched"
	server.listen(1)
	conn, addr = server.accept()

	while True:
		handle(conn)

